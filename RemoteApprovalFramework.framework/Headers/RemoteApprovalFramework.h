//
//  RemoteApprovalFramework.h
//  RemoteApprovalFramework
//
//  Created by Sudarsono Sihotang on 19/01/21.
//

#import <Foundation/Foundation.h>
#import "WBCSDKWrapper.h"
#import "WBCSDKErrorCodesWrapper.h"
#import "WBCSDKConstantsWrapper.h"
#import "SecureStorageSDKWrapper.h"

//! Project version number for RemoteApprovalFramework.
FOUNDATION_EXPORT double RemoteApprovalFrameworkVersionNumber;

//! Project version string for RemoteApprovalFramework.
FOUNDATION_EXPORT const unsigned char RemoteApprovalFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RemoteApprovalFramework/PublicHeader.h>


