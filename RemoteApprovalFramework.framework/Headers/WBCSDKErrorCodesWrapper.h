// (c) 1999 - 2020 OneSpan North America Inc. All rights reserved.


/////////////////////////////////////////////////////////////////////////////
//
//
// This file is example source code. It is provided for your information and
// assistance. See your licence agreement for details and the terms and
// conditions of the licence which governs the use of the source code. By using
// such source code you will be accepting these terms and conditions. If you do
// not wish to accept these terms and conditions, DO NOT OPEN THE FILE OR USE
// THE SOURCE CODE.
//
// Note that there is NO WARRANTY.
//
//////////////////////////////////////////////////////////////////////////////



#import <Foundation/Foundation.h>

/** Internal error */
FOUNDATION_EXPORT NSInteger const INTERNAL_ERROR;

/** The crypto mechanism is invalid. */
FOUNDATION_EXPORT NSInteger const CRYPTO_MECHANISM_INVALID;

/** The crypto mode is invalid. */
FOUNDATION_EXPORT NSInteger const CRYPTO_MODE_INVALID;

/** The WBCSDKTable object is invalid. */
FOUNDATION_EXPORT NSInteger const WBCSDK_TABLES_INVALID;

/** The initial vector byte array is null. */
FOUNDATION_EXPORT NSInteger const INITIAL_VECTOR_NULL;

/** The initial vector length is incorrect according to the selected mechanism. */
FOUNDATION_EXPORT NSInteger const INITIAL_VECTOR_INCORRECT_LENGTH;

/** The input byte array is null. */
FOUNDATION_EXPORT NSInteger const INPUT_DATA_NULL;

/** The input bytes array length is incorrect according to the selected mechanism. */
FOUNDATION_EXPORT NSInteger const INPUT_DATA_INCORRECT_LENGTH;

/** The output byte array is null. */
FOUNDATION_EXPORT NSInteger const OUTPUT_DATA_NULL;

/** The output data length is invalid. */
FOUNDATION_EXPORT NSInteger const OUTPUT_DATA_INCORRECT_LENGTH;
