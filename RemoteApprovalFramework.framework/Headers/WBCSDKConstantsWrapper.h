// (c) 1999 - 2020 OneSpan North America Inc. All rights reserved.


/////////////////////////////////////////////////////////////////////////////
//
//
// This file is example source code. It is provided for your information and
// assistance. See your licence agreement for details and the terms and
// conditions of the licence which governs the use of the source code. By using
// such source code you will be accepting these terms and conditions. If you do
// not wish to accept these terms and conditions, DO NOT OPEN THE FILE OR USE
// THE SOURCE CODE.
//
// Note that there is NO WARRANTY.
//
//////////////////////////////////////////////////////////////////////////////



#import <Foundation/Foundation.h>

/** AES cipher mechanism */
FOUNDATION_EXPORT unsigned char const CRYPTO_MECHANISM_AES;

/** CTR cipher mode */
FOUNDATION_EXPORT unsigned char const CRYPTO_MODE_CTR;
